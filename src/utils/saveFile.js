const fs = require("fs");
const path = require("path");
const uuid = require('uuid');
const { promisify } = require("util");
const writeFileAsync = promisify(fs.writeFile);
const decode = require('./decode')

exports.saveImageToDisk = async (baseImage,basePath) => {
  //find path of project
  const projectPath = path.resolve("./");
  const uploadPath = `${projectPath}${basePath}`;

  //find file extenstion
  const ext = baseImage.substring(
    baseImage.indexOf("/") + 1,
    baseImage.indexOf(";base64")
  );

  //Random new file name with extensions.
  let filename = "";
  if (ext === "svg+xml") {
    filename = `${uuid.v4()}.svg`;
  } else {
    filename = `${uuid.v4()}.${ext}`;
  }

  //Extract base64 data 
  let image = decode.decodeBase64Image(baseImage);

  await writeFileAsync(uploadPath + filename, image.data, "base64");
  return filename;
};
