const express = require("express");
const router = express.Router();
const index = require("../configs/path");
const testController = require("../controllers/testController");
const accountController = require('../controllers/accountController');
const productController = require('../controllers/productController');

/**
 * /api/tests
 */
router.use(index.tests, testController);
/**
 * /api/accounts
 */
router.use(index.accounts, accountController);
/**
 * /api/products
 */
router.use(index.products, productController);

module.exports = router;
