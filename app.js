const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const config = require("./src/configs/index");
const indexRouter = require("./src/routes/index");
const mongoose = require("mongoose");

const app = express();

/** connection mongodb */
mongoose.connect(config.CONNECTION_STRING, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

app.use(logger("dev"));
app.use(express.json({
  "limit":"50mb"
}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use(indexRouter);

module.exports = app;
