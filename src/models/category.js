const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categorySchema = new Schema(
  {
    categoryName: String
  },
  {
    tiemstamps: true,
    collection: "Categories",
  }
);

const category = mongoose.model("Category", categorySchema);

module.exports = category;
