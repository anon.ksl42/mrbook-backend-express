const express = require("express");
const router = express.Router();
const config = require("../configs/index");
const productService = require("../services/productService");

router.get("/", async (req, res, next) => {
  try {
    const result = await productService.GetAllProduct();
    result.map((product) => {
      product.imageUrl = `${config.DOMAIN_IMAGEURI}${product.imageUrl}`;
    });
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json(error.message);
  }
});

router.post("/", async (req, res, next) => {
  try {
    const result = await productService.AddProduct(req);
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json(error.message);
  }
});

module.exports = router;
