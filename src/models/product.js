const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = new Schema(
  {
    title: { type: String, trim: true },
    description: { type: String, trim: true },
    imageUrl: { type: String, default: "nopic.jpeg" },
    price: { type: Number },
    category: { type: Schema.Types.ObjectId, ref: "Category" },
    // createAt: { type: Date, default: Date.now() },
    status: { type: String },
  },
  {
    toJSON: { virtuals: true },
    tiemstamps: true,
    collection: "Products",
  }
);

productSchema.virtual("price_vat").get(function () {
  return this.price * 0.07 + this.price;
});

const product = mongoose.model("Product", productSchema);

module.exports = product;
