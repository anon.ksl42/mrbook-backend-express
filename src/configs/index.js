require("dotenv").config();

module.exports = {
  PORT: process.env.PORT,
  CONNECTION_STRING: process.env.MONGODB_URI,
  DOMAIN_IMAGEURI: process.env.DOMAIN_IMAGEURI,
  PATH_IMAGE:process.env.PATH_IMAGE,
  PATH_PDF:process.env.PATH_PDF
};
