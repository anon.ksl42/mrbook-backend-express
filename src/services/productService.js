const config = require("../configs/index");
const Category = require("../models/category");
const Product = require("../models/product");
const { saveImageToDisk } = require("../utils/saveFile");

exports.GetAllProduct = async () => {
  return await Product.find().populate("category").select("-__v");
};
exports.AddProduct = async (req) => {
  if (req.body.imageUrl !== "" || req.body.imageUrl !== undefined) {
    req.body.imageUrl = await saveImageToDisk(
      req.body.imageUrl,
      config.PATH_IMAGE
    );
  }
  console.log('req.body.imageUrl  :>> ', req.body.imageUrl );
  let product = new Product(req.body);
  await product.save();
  return "insert success";
};
